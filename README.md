## Reese Address Book

This file contains usage instructions, design outline and working notes during development. 

## Usage

The code can be built, tests run and code coverage reports generated by running:

~~~
mvn clean install cobertura:cobertura
~~~

Coverage reports are then visible in ./target/site/cobertura/index.html

## General Design 

The final design has an implementation of a repository and a business service. Access to the components in the solution is centralized in a Factory. Implementations of the repository, the data item, and the business service are held in separate packages with only interfaces held at the root package level.


### External API 

#### AddressBookFactory

Centralizes creation of all objects for the module

#### AddressBookService

Defines all the externally visible methods

#### AddressBook

Defines the persistent unit. Internally a map, with very circumscribed set of exposed methods

#### Contact

The data for a single contact. Full Name is used as the prime key

### Repository 

#### ContactImpl  

Implements the interface defined in Contact. Only two attributes. Full name is a String. Phone Numbers is a list of Strings 

#### AddressBookimpl

Implements the interface defined in AddressBook. Essentially a wrapper around a map from prime key (full name) to the contact object. This allows the unique property on a merged set of address books to be implemented simply at this level.

### Service Layer

#### AddressBookServiceImpl

Implements the methods defined in AddressBookService. Mostly wraps methods in the repository but adds detailed code to support printing

## Thoughts on the Requirements Definition

Q1. Should the data item - 'Contact' - be just the data items required or a more extensible structure, a key-value map say.

Q2. The important data store - AddressBook - is NOT a singleton in the system. So not a direct map to JPA even if that was mandated. Should the class simply extend a standard collection - e.g. List<Contact> - or be an implementation of a completely custom interface.

Q3. Operations defined include 'print' on an address book, and construction of a new / temporary address book with unique elements. I would like to implement print with a Visitor. Is this best? Definitely extensible. 

Q4 Given the gross package structure (controller/service/repository) should the two operations be defined in the service layer or implemented in the repository? i.e. constructing a unique set of contacts can be done with Java collections classes and helpers inside a constructor or an 'add' or 'merge' method. Which is best?

Q5 Given the data items and the operations required, might it be best that AddressBook is just an iplementation of an EAV database. i.e. one large structure with a single map of 'entity-id + attribute-name' to 'value'. Probalby lead to ugly code, but is always an option even in real data stores and data warehouses, so may be useful here.

Q6 The combination of address books is defined to have a unique quality. Does this imply / allow for unique on a single address book? Should non-unique entries be allowed?

Q7 Edit is not defined. Is that an error? Should that operation be added? Or is it a test and will be discussed later.

Q8. Issue around multiple phone numbers per contact. Should this be supported with custom methods on Cpontact, or simply modify the attributes to be List<String> phoneNumber(s) ? Probably the latter - this is the standard for JPA type collection attributes.

## Working Notes

Work began with the setup of the maven script and the Eclipse project to allow IDE work seamlessly.

Initial packages were added for the first guess at the structure of an efficient solution. 

Typical API implementation would have a persistence layer, a business logic layer and an external presentation layer. This is reflected in the initial package structure under src/main/java. 

No tests at this stage

Observations from requirements stories:
The data items required are just name and phone number
The system must allow multiple address books
Standard CRUD operations on data BUT no edit defined.
The system shall be able to iterate an address book to complete an action (print)
The system shall be able to use multiple address books and complete an action (print) as if there was a single address book with unique names.

2018-09-08 11.28 Right. Lets begin with some code. Start with Contact, as it is the core data item. Will code an interface and separate implementation. Will code test of methods before implementation. i.e. Interface -> Test -> Implementation.

2018-09-08 12.09 Badly written requirement - each contact can have MULTIPLE phone numbers. How to support this? 

2018-09-08 13.50 Start work on AddressBook class.

2018-09-08 14.51 Got a working AddressBook with adAll (implements a Contact merge to ensure uniqueness of prime key) includes a Visitor pattern to allow extensible operation list on data class.

2018-09-08 14.56 Started on service class and interface to support business logic.

First question - is the business logic service a stateless singleton. JEE principles would suggest 'yes' (so the address book(s) far each operation are the parameters to the method) But this means the address books are owned by the calling client. Will proceed with this approach.

2018-09-08 15.57 Finished initial cut of service methods. Added a Factory for the entire module. Need to rationalize so ALL 'creates' go through the factory - otherwise it is pointless! Note: I thought I would leave out 'creates' in the test/ folder, but then if someone was using the test code as example usage, they would copy code not in the style preferred style.

2018-09-08 16.23 First run of Cobertura identified a very small count of untested lines of code. Will add tests on this.

