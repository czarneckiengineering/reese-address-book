package au.com.reese.test.addressbook.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.reese.test.addressbook.AddressBook;
import au.com.reese.test.addressbook.AddressBookFactory;
import au.com.reese.test.addressbook.AddressBookService;
import au.com.reese.test.addressbook.Contact;
import au.com.reese.test.addressbook.repository.ContactVisitor;

public class AddressBookServiceImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createAddressBookAndAddAndRemoveContact() {
		AddressBookService service = AddressBookFactory.createAddressBookService();
		
		AddressBook addressBook = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook);
		
		Contact contact = AddressBookFactory.createContact("Marek", Arrays.asList(new String[] {"0488223747"}));
		
		service.add(addressBook, contact);
		
		ContactVisitor visitor1 = new ContactVisitor();
		addressBook.accept(visitor1);
		assertEquals(1, visitor1.getContacts().size());
		
		service.remove(addressBook, "Marek");
		
		ContactVisitor visitor2 = new ContactVisitor();
		addressBook.accept(visitor2);
		assertEquals(0, visitor2.getContacts().size());
	}

	int outputLineCount = 0;
	
	@Test
	public void createTwoAddressBooksAddSameContactInEachAndPrint() {
		AddressBookService service = AddressBookFactory.createAddressBookService();
		
		AddressBook addressBook1 = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook1);
		
		AddressBook addressBook2 = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook2);
		
		Contact contact = AddressBookFactory.createContact("Marek", Arrays.asList(new String[] {"0488223747"}));
		
		service.add(addressBook1, contact);
		service.add(addressBook2, contact);
		
		ContactVisitor visitor1 = new ContactVisitor();
		addressBook1.accept(visitor1);
		assertEquals(1, visitor1.getContacts().size());

		service.print(new OutputStrategy() {
			
			@Override
			public void println(String output) {
				outputLineCount++;
			}
			
		}, addressBook1, addressBook2);

		assertEquals(4, outputLineCount);
	}

}
