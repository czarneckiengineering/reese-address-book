/**
 * 
 */
package au.com.reese.test.addressbook.repository;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.reese.test.addressbook.AddressBookFactory;
import au.com.reese.test.addressbook.Contact;

/**
 * @author user
 *
 */
public class ContactImplTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createBlankContact() {
		Contact blankContact = AddressBookFactory.createContact();
		assertNotNull(blankContact);
		assertNull(blankContact.getFullName());
		assertNull(blankContact.getPhoneNumbers());
	}

	@Test
	public void createNewContact() {
		Contact newContact = AddressBookFactory.createContact("Marek", Arrays.asList(new String [] {"0488223747"}));
		assertNotNull(newContact);
		assertEquals(newContact.getFullName(), "Marek");
		assertEquals(newContact.getPhoneNumbers(), Arrays.asList(new String [] {"0488223747"}));
	}

	@Test
	public void setAndGetFullNameOnContact() {
		Contact newContact = AddressBookFactory.createContact();
		assertNotNull(newContact);
		
		newContact = newContact.setFullName("Marek");
		assertNotNull(newContact);
		
		assertEquals(newContact.getFullName(), "Marek");
	}

	@Test
	public void setAndGetPhoneNumberOnContact() {
		Contact newContact = AddressBookFactory.createContact();
		assertNotNull(newContact);
		
		newContact = newContact.setPhoneNumbers(Arrays.asList(new String [] {"0488223747"}));
		assertNotNull(newContact);
		
		assertEquals(newContact.getPhoneNumbers(), Arrays.asList(new String [] {"0488223747"}));
	}

	@Test
	public void createBlankContactSetAndGetFluentStyle() {
		Contact newContact = AddressBookFactory.createContact();
		assertNotNull(newContact);
		
		newContact.setFullName("Marek").setPhoneNumbers(Arrays.asList(new String [] {"0488223747"}));
		
		assertEquals(newContact.getFullName(), "Marek");
		assertEquals(newContact.getPhoneNumbers(), Arrays.asList(new String [] {"0488223747"}));
	}

	
}
