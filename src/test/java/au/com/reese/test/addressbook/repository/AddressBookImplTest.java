package au.com.reese.test.addressbook.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.reese.test.addressbook.AddressBook;
import au.com.reese.test.addressbook.AddressBookFactory;
import au.com.reese.test.addressbook.Contact;

public class AddressBookImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBlankAddressBook() {
		AddressBook addressBook = AddressBookFactory.createAddressBook();
		
		assertNotNull(addressBook);
	}

	@Test
	public void createAddressBookAddContactAndThenSameContactWithTwoPhoneNumbers() {
		AddressBook addressBook = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook);
		
		Contact contact1 = AddressBookFactory.createContact().setFullName("Marek").setPhoneNumbers(Arrays.asList(new String[] {"0488223747", "0488223737"}));
		addressBook.add(contact1);
		
		ContactVisitor visitor = new ContactVisitor();
		addressBook.accept(visitor);
		assertEquals(1, visitor.getContacts().size());
		
		Contact contact2 = AddressBookFactory.createContact().setFullName("Marek").setPhoneNumbers(Arrays.asList(new String[] {"0488223747", "0488223757"}));
		addressBook.add(contact2);

		ContactVisitor visitor2 = new ContactVisitor();
		addressBook.accept(visitor2);
		assertEquals(1, visitor2.getContacts().size());
		
		Contact updatedContact = visitor2.getContacts().get(0);
		
		List<String> phoneNumbers = updatedContact.getPhoneNumbers();
		assertEquals(3, phoneNumbers.size());
	}

	@Test
	public void createAddressBookThenAddAllContactsFromSecondAddressBook() {
		AddressBook addressBook1 = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook1);
		
		Contact contact1 = AddressBookFactory.createContact().setFullName("Marek").setPhoneNumbers(Arrays.asList(new String[] {"0488223747"}));
		addressBook1.add(contact1);
		
		ContactVisitor visitor = new ContactVisitor();
		addressBook1.accept(visitor);
		assertEquals(1, visitor.getContacts().size());

		AddressBook addressBook2 = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook2);
		
		Contact contact2 = AddressBookFactory.createContact().setFullName("Marek2").setPhoneNumbers(Arrays.asList(new String[] {"0488223747"}));
		addressBook2.add(contact2);
		
		ContactVisitor visitor2 = new ContactVisitor();
		addressBook2.accept(visitor2);
		assertEquals(1, visitor2.getContacts().size());
		
		addressBook1.addAll(addressBook2);
		
		ContactVisitor visitor3 = new ContactVisitor();
		addressBook1.accept(visitor3);
		assertEquals(2, visitor3.getContacts().size());
	}

	@Test
	public void createAddresBookThenAddContactThenRemoveByPrimeKey() {
		AddressBook addressBook = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook);
		
		Contact contact1 = AddressBookFactory.createContact().setFullName("Marek").setPhoneNumbers(Arrays.asList(new String[] {"0488223747"}));
		addressBook.add(contact1);
		
		ContactVisitor visitor1 = new ContactVisitor();
		addressBook.accept(visitor1);
		assertEquals(1, visitor1.getContacts().size());

		addressBook.removeByPrimeKey("Marek");
		
		ContactVisitor visitor2 = new ContactVisitor();
		addressBook.accept(visitor2);
		assertEquals(0, visitor2.getContacts().size());
	}

	@Test
	public void createEmptyAddresBookThenAddRemoveByPrimeKey() {
		AddressBook addressBook = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook);
		
		addressBook.removeByPrimeKey("Marek");
		
		ContactVisitor visitor2 = new ContactVisitor();
		addressBook.accept(visitor2);
		assertEquals(0, visitor2.getContacts().size());
	}

	@Test
	public void callAccept() {
		AddressBook addressBook = AddressBookFactory.createAddressBook();
		assertNotNull(addressBook);
		
		addressBook.accept(new Visitor() {
			
			@Override
			public void visit(Contact contact) {
				fail("No contact instance to get here");
			}
			
		});
	}
	
}
