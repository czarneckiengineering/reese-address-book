package au.com.reese.test.addressbook.service;

public interface OutputStrategy {
	
	void println(String output);

}
