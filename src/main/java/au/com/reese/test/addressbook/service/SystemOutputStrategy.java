package au.com.reese.test.addressbook.service;

public class SystemOutputStrategy implements OutputStrategy {

	public void println(String output) {
		System.out.println(output);
	}

}
