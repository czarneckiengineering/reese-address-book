package au.com.reese.test.addressbook;

import au.com.reese.test.addressbook.repository.Visitor;

public interface AddressBook {
	
	AddressBook add (Contact contact);
		
	AddressBook addAll (AddressBook addressBook);
	
	AddressBook removeByPrimeKey (String primeKey);

	void accept (Visitor visitor);

}
