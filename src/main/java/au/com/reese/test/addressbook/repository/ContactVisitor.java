package au.com.reese.test.addressbook.repository;

import java.util.ArrayList;
import java.util.List;

import au.com.reese.test.addressbook.AddressBook;
import au.com.reese.test.addressbook.Contact;

public class ContactVisitor implements Visitor {

	private List<Contact> contacts = new ArrayList<Contact> ();

//	@Override/
//	public void visit(AddressBook addressBook) {
//	}

	@Override
	public void visit(Contact contact) {
		contacts.add(contact);
	}

	public List<Contact> getContacts() {
		return contacts ;
	}

}
