package au.com.reese.test.addressbook.repository;

import au.com.reese.test.addressbook.AddressBook;
import au.com.reese.test.addressbook.Contact;

public interface Visitor {
	
//	void visit(AddressBook addressBook);
	
	void visit(Contact contact);

}
