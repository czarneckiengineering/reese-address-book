package au.com.reese.test.addressbook;

import java.util.List;

import au.com.reese.test.addressbook.repository.Visitor;

public interface Contact {
	
	String getPrimeKey();
	
	String getFullName ();
	Contact setFullName(String fullName);
	
	List<String> getPhoneNumbers();
	Contact setPhoneNumbers(List<String> phoneNumbers);

	void accept(Visitor visitor);

}
