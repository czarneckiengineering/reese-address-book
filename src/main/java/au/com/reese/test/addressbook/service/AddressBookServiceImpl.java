package au.com.reese.test.addressbook.service;

import au.com.reese.test.addressbook.AddressBook;
import au.com.reese.test.addressbook.AddressBookFactory;
import au.com.reese.test.addressbook.AddressBookService;
import au.com.reese.test.addressbook.Contact;
import au.com.reese.test.addressbook.repository.ContactVisitor;

public class AddressBookServiceImpl implements AddressBookService {
	
	public static AddressBookServiceImpl create() {return new AddressBookServiceImpl(); }
	
	private AddressBookServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void add(AddressBook addressBook, Contact contact) {
		addressBook.add(contact);
	}

	@Override
	public void remove(AddressBook addressBook, String primeKey) {
		addressBook.removeByPrimeKey(primeKey);
	}

	@Override
	public void print(OutputStrategy outputStrategy, AddressBook... addressBooks) {
		AddressBook newAddressBook = AddressBookFactory.createAddressBook();
		
		for (AddressBook addressBook : addressBooks) {
			newAddressBook.addAll(addressBook);
		}
		
		ContactVisitor visitor = new ContactVisitor();
		
		newAddressBook.accept(visitor);
		
		outputStrategy.println("==============");
		for (Contact contact : visitor.getContacts()) {
			outputStrategy.println(contact.getFullName());
			
			for(String phoneNumer : contact.getPhoneNumbers()) {
				outputStrategy.println(phoneNumer);
			}
			outputStrategy.println("==============");
		}
 	}

}
