package au.com.reese.test.addressbook.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import au.com.reese.test.addressbook.AddressBook;
import au.com.reese.test.addressbook.Contact;

public class AddressBookImpl implements AddressBook {

	private Map<String, Contact> contacts;
	
	public static AddressBook create () {return new AddressBookImpl();}
	
	private AddressBookImpl() {
		contacts = new TreeMap<String, Contact>();
	}

	@Override
	public AddressBook add(Contact newContact) {
		String primeKey = newContact.getPrimeKey();
		
		Contact oldContact = contacts.get(primeKey);
		
		if (oldContact != null) {
			List<String> oldPhoneNumbers = oldContact.getPhoneNumbers();
			List<String> newPhoneNumbers = newContact.getPhoneNumbers();
			
			for (String phoneNumber : oldPhoneNumbers) {
				if (!newPhoneNumbers.contains(phoneNumber)) {
					newPhoneNumbers = new ArrayList<String> (newPhoneNumbers); 
					newPhoneNumbers.add(phoneNumber);
				}
			}
			
			newContact.setPhoneNumbers(newPhoneNumbers);
			
		}
		
		contacts.put(primeKey, newContact);
		
		return this;
	}

	@Override
	public AddressBook addAll(AddressBook addressBook) {
		ContactVisitor visitor = new ContactVisitor();

		addressBook.accept(visitor);
		
		List<Contact> newContacts = visitor.getContacts();
		
		for (Contact newContact : newContacts) {
			add(newContact);
		}
		
		return this;
	}

	@Override
	public AddressBook removeByPrimeKey(String primeKey) {
		Contact contact = contacts.remove(primeKey);
		
		if (contact == null) {
			System.out.println("The specified Contact could not be found in this address Book");
		}
		
		return this;
	}

	@Override
	public void accept(Visitor visitor) {
		for (Contact contact : contacts.values()) {
			contact.accept(visitor);
		}
	}

}
