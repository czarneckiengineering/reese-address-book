package au.com.reese.test.addressbook;

import java.util.List;

import au.com.reese.test.addressbook.repository.AddressBookImpl;
import au.com.reese.test.addressbook.repository.ContactImpl;
import au.com.reese.test.addressbook.service.AddressBookServiceImpl;

public class AddressBookFactory {

	private AddressBookFactory () {
	}
	
	public static AddressBookService createAddressBookService() {
		return AddressBookServiceImpl.create();
	}
	
	public static AddressBook createAddressBook() {
		return AddressBookImpl.create();
	}

	public static Contact createContact () {
		return ContactImpl.create();
	}

	public static Contact createContact (String fullName, List<String> phoneNumbers) {
		return ContactImpl.create(fullName, phoneNumbers);
	}
	
}
