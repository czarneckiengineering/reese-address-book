package au.com.reese.test.addressbook;

import au.com.reese.test.addressbook.service.OutputStrategy;

public interface AddressBookService {
	
	void add (AddressBook addressBook, Contact contact);
	
	void remove (AddressBook addressBook, String primeKey);
	
	void print (OutputStrategy outputStrategy, AddressBook ...addressBooks);

}
