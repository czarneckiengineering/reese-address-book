package au.com.reese.test.addressbook.repository;

import java.util.List;

import au.com.reese.test.addressbook.Contact;

public class ContactImpl implements Contact {
	
	public static Contact create() {return new  ContactImpl();}
	
	public static Contact create(String fullName, List<String> phoneNumbers) {return new  ContactImpl(fullName, phoneNumbers);}

	private String fullName;
	
	private List<String> phoneNumbers;

	private ContactImpl() {
	}

	public ContactImpl(String fullName, List<String> phoneNumbers) {
		this.fullName = fullName;
		this.phoneNumbers = phoneNumbers;
	}

	@Override
	public String getFullName() {
		return fullName;
	}

	@Override
	public Contact setFullName(String fullName) {
		this.fullName = fullName;
		return this;
	}

	@Override
	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	@Override
	public Contact setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
		return this;
	}

	@Override
	public String getPrimeKey() {
		return getFullName();
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

}
